from django.urls import path
#from applications.home. views import IndexView #ya
from . import views

urlpatterns = [
    path('home/' ,views.IndexView.as_view()),
    path('lista/', views.PruebaListView.as_view()),
    path('lista-prueba/', views.ModeloPruebaListView.as_view()),
]
