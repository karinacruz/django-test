from django.shortcuts import render
from django.views.generic import TemplateView, ListView #Agregue la classe ListView
from .models import Prueba
# Create your views here.

class IndexView(TemplateView):
        template_name = './home/home.html'
 
class PruebaListView(ListView):
        template_name = './home/lista.html'
        queryset = ['A','B', 'C']
        context_object_name = 'lista_prueba' #para especificar la variable

 
class ModeloPruebaListView(ListView):
    model = Prueba #Estoy solicitando conectarme a una clase que no esta en este archivo, así qeu voy a mandarlo llamar con from .models import Prueba
    template_name = "./home/listabd.html"
    context_object_name = 'listabd'
