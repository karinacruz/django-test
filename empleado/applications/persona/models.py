from django.db import models
from applications.departamento.models import Departamento
# Create your models here.
class Empleado(models.Model):
    """Modelo para tabla empleado"""

         

job_choices = (
            ('0', 'DESIGNER'),
            ('1', 'PROGRAMADOR'),
            ('2', 'GERENTE'),
            ('3', 'ASESOR'),
        )



first_name = models.CharField('Nombres', max_length=60)
last_name = models.CharField('Apellidos', max_length=60)
job = models.CharField('Puesto', max_length=1, choices=job_choices)
departamento = models.ForeignKey(Departamento, on_delete=models.CASCADE)
# image = models.ImageField(, upload_to=None, height_field=None, width_field=None, max_length=None)

def __star__(self):
    return str(self.id) + '_' + self.first_name + '_' + self.last_name