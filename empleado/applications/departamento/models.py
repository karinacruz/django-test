from django.db import models

# Create your models here.

class Departamento(models.Model):#Representacion de la base de datos, para que se vea en el gestor de base de datos es PREMIGRACION python manage.py makemigrations y para generar la base de datos es python manage.py migrate
    name = models.CharField('Nombre', max_length=20, blank=True)
    shor_name = models.CharField('Nombre corto', max_length=50, unique=True)
    anulate = models.BooleanField('Anulado', default=False)

def __star__(self):
    return str(self.id) + '_' + self.name + '_' + self.shorname